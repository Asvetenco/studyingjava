package pro.java.robots;
import static pro.java.util.Print.*;

public class Robot {

	private int serialNumber;
	protected String name = "NoNaMe";
	
	Robot(){
		println("Robot()");
	}
	
	Robot(String name) {
		this.name = name;
	}

	protected int getSerialNumber() {
		return serialNumber;
	}
	
	protected String getName(){
		return name;
	}
	
	protected void setSerialNumber(int sn){
		serialNumber = sn;
	}
	
	protected void setName (String name){
		this.name = name;
	}

}
