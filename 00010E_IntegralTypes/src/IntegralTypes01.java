import static pro.java.util.Print.*;
public class IntegralTypes01 {
	// примеры операций с целочисленными типами
	public static void main(String[] args) {
		//если раскомментировать строку ниже, то компилятор 
		// или IDE покажет ошибку
		//long iLong = 2147483648;
		long iLong = 2147483648L; // а здесь уже все нормально
		println("iLong="+iLong); 
		byte a, b;
		a=5;
		b=3;
		println("a="+a+"   b="+b);
		println("a+b="+(a+b));
		println("a-b="+(a-b));
		println("a*b="+(a*b));
		println("a/b="+(a/b));
		print("a%b="+(a%b));
		println(" тоже самое что a-(a/b)*b="+(a-(a/b)*b));
		println();
		println("a="+a+"   b="+b);
		println("Постфиксный инкремент a++. Сейчас a="+a++);
		println("И только сейчас a="+a);
		println("Постфиксный декремент a--. Сейчас a="+a--);
		println("И только сейчас a="+a);
		println("Префиксный инкремент ++a. Сейчас a="+(++a));
		println("Префиксный декремент --a. Сейчас a="+(--a));
		a+=b; // равнозначно a=a+b
		println("После операции a+=b  a="+a);
		a-=b; // равнозначно a=a-b
		println("После операции a-=b  a="+a);
		byte c=127;
		++c; // по существу 
		++c; // это все равно что 127+2=129
		println("Демонстрация переполнения c="+c);
	}
}
