package pro.java.supersub;

import static pro.java.util.Print.*;

class Super {
	String str = "ClassSuper";
	String getStr() { return str; }
}

class Sub extends Super {
	String str = "ClassSub";
	String getStr() { return str; }
	String getSuperStr() { return super.str; }
}

public class Main {

	public static void main(String[] args) {
		// Полиморфизм. Прямое обращение к полю.
		Super sup = new Sub();
		println("sup.str = " + sup.str 
				+ "  sup.getStr() = " + sup.getStr());
		println();
		Sub sub = new Sub();
		println("sub.str = " + sub.str 
				+ "  sub.getStr() = " + sub.getStr() 
				+ "  \nsub.getSuperStr() = " 
				+ sub.getSuperStr());
	}
}
