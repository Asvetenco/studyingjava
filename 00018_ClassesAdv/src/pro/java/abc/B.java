package pro.java.abc;

import static pro.java.util.Print.*;

class B extends A {
	String x = "Класс B";
	
	void printB() {
		println("in method B");
		super.printX();
		printLnLineLn();
	}
	
	void printX(){
		print("X->" + x);
	}
}
