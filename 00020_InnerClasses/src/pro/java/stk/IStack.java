package pro.java.stk;

import static pro.java.util.Print.*;

public interface IStack {

	void push(Object obj); // помещаем в стек
	Object pop(); // извлекаем из стека
	void printStack(); // печатем стек

	class FixedStack implements IStack {

		private static Object[] stk;
		private static int tos;

		// создание стека указанного размера
		public FixedStack(int size) {
			stk = new Object[size];
			tos = -1;
		}

		public void push(Object obj) {
			if (tos == stk.length - 1)
				println("Стек полон!");
			else
				stk[++tos] = obj;
		}

		public Object pop() {
			if (tos < 0)
				return null;
			else
				return stk[tos--];
		}

		public void printStack() {
			if (tos < 0) {
				println("Стек пуст!");
				return;
			}
			for (int i = tos; i > -1; --i) {
				println("Stack [" + i + "] = " + stk[i]);
			}
		}

		static class DynStack extends FixedStack {

			public DynStack(int size) {
				super(size);
			}

			@Override
			public void push(Object obj) {

				if (tos == stk.length - 1) {
					Object temp[] = new Object[stk.length * 2];
					for (int i = 0; i < stk.length; i++)
						temp[i] = stk[i];
					stk = temp;
					println("Размер стека увеличился");
					stk[++tos] = obj;
				} else
					stk[++tos] = obj;
			}
		}
	}
}
