import static pro.java.util.Print.*;

public class CharacterWrapper {

	public static void main(String[] args) {
		// пример класса-обертки Character

		if (args.length > 0) {
			for (char w : args[0].toCharArray()) {
				if (Character.isDigit(w)) {
					print(w+" - это число\n");
					continue;
				}
				print(w+" - это символ\n");
			}
		}
	}
}
