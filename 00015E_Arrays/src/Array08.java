import static pro.java.util.Print.*;
import java.util.Arrays;

public class Array08 {

	public static void main(String[] args) {
		// Примеры многомерных массивов Java
		
		int[][] triangle = { { 1, 2, 3, 4, 5 },
							 { 6, 7, 8, 9 },
							 { 10, 11, 12 },
							 { 13, 14 },
							 { 15 } };
		
		println("triangle array");
		println(Arrays.deepToString(triangle));
		println();
		for (int i=0; i<triangle.length; ++i){
			for(int j=0;j<triangle[i].length;++j){
				print(" "+triangle[i][j] );
			}
			println();
		}
	}
}
