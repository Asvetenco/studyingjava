package pro.java.pkg002;
import static pro.java.util.Print.*;

//модификатор доступа к классу по умолчанию
class DefMod {

	static int defInt = 108;
	
	static void defPrtLine(){
		println("--- defPrtLine ---");
	}
}
