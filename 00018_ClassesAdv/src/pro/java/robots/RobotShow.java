package pro.java.robots;
import static pro.java.util.Print.*;

public class RobotShow {

	public static void main(String[] args) {
		
		Robot rb = new Robot("NoNaMe");
		println("rb.getName = " + rb.getName());
		
		RobotCleaner rc = new RobotCleaner();
		println("rc.getName = " + rc.getName());
		rc.printName();
		
	}

}
