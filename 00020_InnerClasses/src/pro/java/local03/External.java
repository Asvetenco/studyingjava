package pro.java.local03;

import static pro.java.util.Print.*;

interface GetLocalClass {
	String getLocalStr();
	String getStr();
}

class Ext {	String str = "Ext"; }

public class External {
	String str = "External";

	GetLocalClass getLocal(final String argstr) {

		class Local extends Ext implements GetLocalClass {
			String str = "Local";

			public String getLocalStr() { return argstr; }

			public String getStr() {
				return str + " " 
						+ External.this.str 
						+ " " + super.str;
			}
		}

		println("Метод getLocal() отработал.");
		return new Local();
	}

}
