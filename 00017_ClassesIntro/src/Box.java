import static pro.java.util.Print.*;

class Box {

	double width;
	double height;
	double depth;
	String label = "Box";

	// конструктор принмающий три параметра
	Box(double w, double h, double d) {
		width = w;
		height = h;
		depth = d;
	}

	// конструктор без параметров (по умолчанию)
	Box() {
	}

	// конструктор принимающий один параметр
	Box(double size) {
		width = height = depth = size;
	}

	// метод вывода объема на консоль
	void printVolume() {
		print(width * height * depth);
	}

	// метод вывода размеров Box на консоль
	void printSizes() {
		println("width  = " + width);
		println("height = " + height);
		println("depth  = " + depth);
	}

	// метод получения объема, возвращает double
	double getVolume() {
		return width * height * depth;
	}

	// задаем один и тот же размер
	void setSameSize(double size) {
		width = height = depth = size;
	}

	// задаем все размеры
	void setSizes(double w, double h, double d) {
		width = w;
		height = h;
		depth = d;
	}

	// перегруженный метод setData
	void setData(double size) {
		width = height = depth = size;
	}
	
	void setData(double w, double h, double d) {
		width = w;
		height = h;
		depth = d;
	}
	
	void setData(String boxLabel){
		label = boxLabel;
	}

}
