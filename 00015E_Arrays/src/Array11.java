import static pro.java.util.Numbers.*;
import static pro.java.util.Print.*;

import java.util.Arrays;

public class Array11 {

	public static void main(String[] args) {
		// Примеры работы с массивами в Java

		int[] a = { 1, 2 };
		int[] b = { 1, 2 };

		println("a==b is " + (a == b)); // сравнивает ссылки
		println("a.equals(b) is " + a.equals(b)); // сравнивает ссылки
		println("a=" + a + "    b=" + b);
		println("Arrays.equals(a,b) is " + Arrays.equals(a, b));

		int[][] aa = { { 11, 22 }, { 33, 44 } };
		int[][] bb = { { 11, 22 }, { 33, 44 } };
		println("Arrays.equals(aa,bb) is " + Arrays.deepEquals(aa, bb));

		int[] intArray = new int[10];

		// заполняем массив случайными числами
		for (int i = 0; i < intArray.length; ++i)
			intArray[i] = randomInRange(1, 100);
		
		print("Массив intArray до сортировки: " + Arrays.toString(intArray));
		Arrays.sort(intArray); //сортируем массив
		print("\nМассив intArray после сортировки: " + Arrays.toString(intArray));
		
		int index = Arrays.binarySearch(a, 2); //ищем двойку в массиве a
		println("\nindex="+index); //получаем индекс 2 в массиве а

	}
}
