import static pro.java.util.Print.*;

public class String011 {

	public static void main(String[] args) {
		// примеры создания строк класса StringBuffer

		StringBuffer strBfr1 = new StringBuffer("Подстрочка");
		println("substring(int) = " + strBfr1.substring(3));
		println("substring(int, int) = " + strBfr1.substring(0, 3));

		StringBuffer strBfr2 = new StringBuffer().appendCodePoint(128640);
		strBfr2.append("---");
		strBfr2.appendCodePoint(128648);
		println("\nstrBfr2 = " + strBfr2.toString());
		println("strBfr2.length() = " + strBfr2.length());
		println("strBfr2.codePointCount() = "
				+ strBfr2.codePointCount(0, strBfr2.length()));

		println("\ncodePint at 5 = " + strBfr2.codePointAt(5));
		println("codePint before 6 = "
				+ Integer.toHexString(strBfr2.codePointBefore(6)));
		println("codePint at 6 = "
				+ Integer.toHexString(strBfr2.codePointAt(6)));
		
		println("\nstrBfr2.capacity() = " + strBfr2.capacity());
		strBfr2.trimToSize();
		println("after trim");
		println("strBfr2.capacity() = " + strBfr2.capacity());

	}
}
