package pro.java.create;
import static pro.java.util.Print.*;

class MyString {
	String myStr;

	MyString(String s) {
		println("MyString constructor s = " + s);
		myStr = s;
	}
	public String toString() { return myStr; }
}

class Sup {
	String set = "MySuperString";
	Sup() {
		println("Sup() перед вызовом display()");
		display();
		println("Sup() после вызова display()");
	}
	void display() { println("Sup set = " + set); }
}

class Sub extends Sup{
	{ println("инициализационный блок 1"); }
	String set = new MyString("MySubString").toString();
	{ println("инициализационный блок 2"); }
	void display() { println("Sub set = " + set); }
	Sub() {
		println("Sub() перед вызовом display()");
		display();
		println("Sub() после вызова display()");
	}
}

public class Main {

	public static void main(String[] args) {
		new Sub();
	}
}
