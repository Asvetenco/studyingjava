package pro.java.anonym02;

import static pro.java.util.Print.*;

class External {
	External() { };
	Object getThis() { return this; }
	static External iext = new External(){ };
}

public class Main {

	public static void main(String[] args) {
		
		External ext = new External();
		println("ext.this = " + ext.getThis());
		
		External extanonym = External.iext;
		println("extanonym1.this = " + extanonym.getThis());

	}

}
