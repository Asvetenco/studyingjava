package pro.java.nestext;

class Outer {

	private static String outerName = "outerName";

	Outer() {
		outerName = "defaultNameFromOuter";
	}

	String getName() {
		return outerName;
	}

	static class Nested {

		Nested() {
			outerName = "defaultNameFromNested";
		}

		String getName() {
			return outerName;
		}

		static class Ext1 extends Outer.Nested {

			Ext1() {
				outerName = "defaultNameFromExt1";
			}

			String getName() {
				return outerName;
			}

		}

	}

}

class Ext2 extends Outer.Nested {

	String getName() {
		// return outerName; // нет доступа
		return super.getName() + " !!! from EXT2 !!!";
	}
}
