import static pro.java.util.Print.*;

public class Operators04Break01 {

	public static void main(String[] args) {
		// примеры оператора break

		for (int i = 0; i < 100; i++) {
			if (i == 3)
				break; // выход из цикла если i равно 3
			println("i: " + i);
		}
		println("Цикл завершен.");

		for (int i = 1; i < 4; ++i) {
			print("\nВнешний цикл. Итерация " + i + "\nВнутренний цикл:");
			for (int j = 1; j < 100; ++j) {
				if (j > 4) break;
				print(" " + j);
			}
		}

		extloop: for (int i = 1; i < 4; ++i) {
			print("\n\nВнешний extloop цикл. Итерация " + i + "\nВнутренний цикл:");
			for (int j = 1; j < 100; ++j) {
				if (j > 8) break extloop;
				print(" " + j);
			}
		}
	}
}
