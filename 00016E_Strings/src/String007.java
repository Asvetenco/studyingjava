import static pro.java.util.Print.*;

public class String007 {

	public static void main(String[] args) {
		// примеры создания строк класса StringBuilder

		StringBuilder strBld1 = new StringBuilder();
		println("strBld1.length() = " + strBld1.length());
		println("strBld1.capacity() = " + strBld1.capacity());

		StringBuilder strBld2 = new StringBuilder(10);
		println("\nstrBld2.length() = " + strBld2.length());
		println("strBld2.capacity() = " + strBld2.capacity());

		StringBuilder strBld3 = new StringBuilder("Hello");
		println("\nstrBld3 = " + strBld3);
		println("strBld3.length() = " + strBld3.length());
		println("strBld3.capacity() = " + strBld3.capacity());

	}
}
