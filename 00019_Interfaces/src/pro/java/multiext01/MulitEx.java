package pro.java.multiext01;

import static pro.java.util.Print.*;

interface A {
	default void methA1() {
		println("Method A1");
	}

	default void methA2() {
		println("Method A2");
	}
}

interface B extends A {
	void methB1();
}

class Ext1 implements B {
	@Override
	public void methB1() {
		print("Метод В1");
	}
}

class Ext2 implements B {

	@Override
	public void methA1() {
		println("Метод A1");
	};
	
	@Override
	public void methA2() {
		println("Метод A2");
	};

	@Override
	public void methB1() {
		print("Метод В1");
	}

}

public class MulitEx {

	public static void main(String[] args) {

		Ext1 ext1 = new Ext1();
		ext1.methA1();
		ext1.methA2();
		ext1.methB1();
		printLnLineLn();
		B ext2 = new Ext2(); // внимание сюда
		ext2.methA1();
		ext2.methA2();
		ext2.methB1();
		A ext3 = new Ext2(); // внимание сюда
		printLnLineLn();
		ext3.methA1();
		ext3.methA2();
		// ext3.methB1(); // ОШИБКА
	}

}
