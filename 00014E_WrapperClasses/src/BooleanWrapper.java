import static pro.java.util.Print.*;

public class BooleanWrapper {

	public static void main(String[] args) {
		// пример класса-обертки Boolean

		Boolean wBoolean = false;

		if (args.length > 0)
			wBoolean = Boolean.parseBoolean(args[0]);

		println("wBoolean is " + wBoolean.toString());
	}
}
