package pro.java.stack;

//Определение интерфейса целочисленного стека
public interface IntStack {

	void push(int item); // поместить в стек

	int рор(); // извлечь из стека

	void printStack(); // печать стека

}
