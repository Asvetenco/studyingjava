package pro.java.animal;

abstract class Animal { // абстрактный класс

	private String type;

	abstract void getSound(); // абстрактный метод

	Animal(String aType) {
		type = aType;
	}

	String getType() {
		return type;
	}

}
