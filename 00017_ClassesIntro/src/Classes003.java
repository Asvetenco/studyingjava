import static pro.java.util.Print.*;

public class Classes003 {
	
	public static void main(String[] args) {
		
		Box box1 = new Box();
		box1.setData(15);
		box1.setData("Box1");
		println("box1.depth = " + box1.depth);
		println("box1.label = " + box1.label);
		
		printLine();
		println();
		
		Box box2 = new Box();
		box2.setData(1, 2, 3);
		box2.setData("Box2");
		println("box2.depth = " + box2.depth);
		println("box2.depth = " + box2.label);
		
	}

}
