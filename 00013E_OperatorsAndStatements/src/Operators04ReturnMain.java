import static pro.java.util.Print.*;
public class Operators04ReturnMain {

	public static void main(String[] args) {
		// пример оператора return
		
		println("Вызываем vReturn(3)");
		Operators04ReturnMethod.vReturn(3);
		println("Вызываем vReturn(7)");
		Operators04ReturnMethod.vReturn(7);
		println();
		
		println("Вызываем strReturn(7)  "+Operators04ReturnMethod.strReturn(7));
		println("Вызываем strReturn(3)  "+Operators04ReturnMethod.strReturn(3));
		println("Вызываем strReturn(5)  "+Operators04ReturnMethod.strReturn(5));
		
	}
}
