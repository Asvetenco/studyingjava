import static pro.java.util.Print.*;

public class Classes007 {

	static void сhange(int i) {
		println("В методе change");
		i = 5;
	}

	static void сhange(Box b) {
		println("В методе change");
		b.setData("BOX1");
	}

	static void chnge(Box b) {
		println("В методе chhge");
		b = new Box();
		b.setData("NEW BOX");
	}

	public static void main(String[] args) {
		// примеры передачи аргументов в методы

		int a = 10;
		println("До вызова метода a = " + a);
		сhange(a);
		println("После вызова метода a = " + a);
		println();

		Box box1 = new Box();
		box1.setData("box1");
		println("До вызова метода box1.label = " + box1.label);
		сhange(box1);
		println("После вызова метода box1.label = " + box1.label);
		println();

		Box box2 = new Box();
		box2.setData("box2");
		println("До вызова метода box2.label = " + box2.label);
		chnge(box2);
		println("После вызова метода box2.label = " + box2.label);

	}

}
