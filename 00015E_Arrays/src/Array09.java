import static pro.java.util.Print.*;
import java.util.Arrays;

public class Array09 {

	public static void main(String[] args) {
		// Примеры многомерных массивов Java
		
		int[] one = {1};
		int[][] two ={{2,2},{2,2}};
		int[][] three ={{3,3,3},{3,3,3},{3,3,3}};
		println("one: "+Arrays.toString(one));
				
		println("\ntwo");
		for (int i=0; i<two.length; ++i){
			for(int j=0;j<two[i].length;++j){
				print(" "+two[i][j] );
			}
			println();
		}
		
		println("\nthree");
		for (int i=0; i<three.length; ++i){
			for(int j=0;j<three[i].length;++j){
				print(" "+three[i][j] );
			}
			println();
		}
		
		// начинается магия
		three[1]=two[0];
		three[2]=one;
		
		println("\nthree after magic");
		for (int i=0; i<three.length; ++i){
			for(int j=0;j<three[i].length;++j){
				print(" "+three[i][j] );
			}
			println();
		}
	}
}
